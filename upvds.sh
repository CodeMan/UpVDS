#!/bin/bash

#####################################################################################
#  ______     ______     _____     ______        __    __     ______     __   __    #
# /\  ___\   /\  __ \   /\  __-.  /\  ___\      /\ "-./  \   /\  __ \   /\ "-.\ \   #
# \ \ \____  \ \ \/\ \  \ \ \/\ \ \ \  __\      \ \ \-./\ \  \ \  __ \  \ \ \-.  \  #
#  \ \_____\  \ \_____\  \ \____-  \ \_____\     \ \_\ \ \_\  \ \_\ \_\  \ \_\\"\_\ #
#   \/_____/   \/_____/   \/____/   \/_____/      \/_/  \/_/   \/_/\/_/   \/_/ \/_/ #
#                                                                                   #
#   git: https://codeberg.org/CodeMan                                               #
#   Mastodon: https://techhub.social/@codeman                                       #
#   E-mail: codemansrc@gmail.com                                                    #
#   License: GPLv3                                                                  #
#####################################################################################

#=====================================================================
# STATIC VARIABLES

# Colors for print
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
CYAN='\033[0;36m'
BROR='\033[0;33m'
LGRAY='\033[0;37m'
PURPLE='\033[0;35m'
YELLOW='\033[1;33m'

# Logo
LOGO="
 ______     ______     ______   __  __     ______      __   __   _____     ______    
/\  ___\   /\  ___\   /\__  _\ /\ \/\ \   /\  == \    /\ \ / /  /\  __-.  /\  ___\   
\ \___  \  \ \  __\   \/_/\ \/ \ \ \_\ \  \ \  _-/    \ \ \'/   \ \ \/\ \ \ \___  \  
 \/\_____\  \ \_____\    \ \_\  \ \_____\  \ \_\       \ \__|    \ \____-  \/\_____\ 
  \/_____/   \/_____/     \/_/   \/_____/   \/_/        \/_/      \/____/   \/_____/ 
"

# Unicode symbol
SR="${GREEN}\\u221A"
RQM="${BLUE}\\u00BB"
BDH="${BROR}\\u2550"
GCLT="${CYAN}\\u00A4"
LINE=""

# END STATIC
#=====================================================================

#=====================================================================
# EDITABLE VARIABLES

# Name for create new user
USER="codeman"

# List packages for install
packages="zsh git curl nodejs npm neovim ranger bottom lsd neofetch tmux bat ripgrep lua lazygit gdu sshguard"

# New port for SSHD
PORTSSHD="666"

# Additional locale for the system
LOCALE="ru_RU.UTF-8 UTF-8"

# END EDITABLE
#======================================================================

#=====================================================================
# FUNCTIONS BLOCK

# Generated separet line
for i in {1..84}; do
	LINE=$BDH"$LINE"
done

# Output separet line
function line() {
	echo -e "$LINE\n"
}

# END FUNCTIONS BLOCK
#======================================================================

#=====================================================================
# RUN SCRIPT

# Clear screen
clear

echo -e "${CYAN}${LOGO}"
line

# Preparing system
if [ $(id -u) = 0 ]; then
	if ! test -f "/root/.upvds_um"; then
		echo -e "${RQM} Preparing your system, update and installing packages"
		pacman -Suy --noconfirm &>/dev/null
		pacman -S --noconfirm "$packages" &>/dev/null
		touch /root/.upvds_um
		# Check user and add user if not exists
		echo -e "${RQM} Checking user exists ${USER}"
		if id -u "$USER" >/dev/null 2>&1; then
			echo -e " ${SR} ${USER} exists."
			sudo pacman -Su &>/dev/null
			clear
			echo -e "${CYAN}${LOGO}"
			line
		else
			echo -e " ${SR} Create user ${USER}"
			groupadd admin >/dev/null
			echo -ne "\r"
			useradd -m -g admin -s /bin/zsh ${USER} >/dev/null
			echo "%admin ALL=(ALL) ALL" >>/etc/sudoers
			echo -ne "\r"
			passwd ${USER}
			cp "/root/upvds.sh" "/home/${USER}/upvds.sh" &>/dev/null
			chown "${USER}" "/home/${USER}/upvds.sh"
		fi
		echo -e "  ${SR} Your system is updated and now reboot."
		echo -e "  ${SR} After boot system, Login as ${USER} and run script agane."
		line
		sleep 5
		reboot
		sleep 3
	fi
fi

# Install Oh My Zsh
if test -d "~/.oh-my-zsh/"; then
	echo -e "${SR} Oh my zsh installed"
else
	echo -e "${RQM} Install and settings Oh My Zsh"
	#wget "https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh" &>/dev/null
	#sed '/exec zsh -l/d' &>/dev/null
	#sh -c install.sh
	sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
	git clone "https://github.com/zsh-users/zsh-history-substring-search" "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}"/plugins/zsh-history-substring-search &>/dev/null
	git clone "https://github.com/zsh-users/zsh-autosuggestions" "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}"/plugins/zsh-autosuggestions &>/dev/null
	git clone "https://github.com/zsh-users/zsh-syntax-highlighting.git" "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}"/plugins/zsh-syntax-highlighting &>/dev/null
	sed -i 's/plugins=(git)/plugins=(git zsh-syntax-highlighting zsh-autosuggestions zsh-history-substring-search)' .zshrc &>/dev/null
	sed -i 's/# Example aliases/ # Example aliasess\nalias ipkg="sudo pacman -Sy"\nalias rpkg="sudo pacman -Rcns"\nalias upkg="sudo pacman -Su"\nalias fm="ranger"\nalias sysi="neofetch"\nalias lla="lsd -a"\nalias lt="lsd --tree"\nalias la="lsd"' &>/dev/null
fi

# Install p10k theme
if ! test -d "~/.local/share/fonts/"; then
	echo -e "${RQM} Install p10k theme for Oh My Zsh."
	mkdir -p "~/.local/share/fonts" &>/dev/null
	cd "~/.local/share/fonts/" || return &>/dev/null
	wget "https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf" &>/dev/null
	wget "https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf" &>/dev/null
	wget "https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf" &>/dev/null
	wget "https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf" &>/dev/null
	git clone --depth=1 "https://github.com/romkatv/powerlevel10k.git" "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}"/themes/powerlevel10k &>/dev/null
	sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="powerlevel10k/powerlevel10k"' &>/dev/null
else
	echo -e "${SR} p10k theme installed for Oh My Zsh"
fi

# Install AstroVim
if ! test -d "~/.config"; then
	echo -e "${RQM} Install AstroVim"
	mkdir -p "~/.config" &>/dev/null
	git clone "https://github.com/AstroNvim/AstroNvim" ~/.config/nvim &>/dev/null
	npm install tree-sitter-cli &>/dev/null
else
	echo -e "${SR} AstroVim installed"
fi

# End installing and settings zsh, plugins and astrovim
echo -e "  ${SR} Oh My ZSH installed, now you need logout, login agaen and run script."
line
sleep 3
exit
sleep 3

# Get sudo password for user
read -sp "${RQM} Please input sudo password for $(whoami): " PASSWD
line

# Preparing system
echo -e "${RQM} Preparing your system, updating mirrors and installing updates"
echo "$PASSWD" | sudo pacman -Suy --noconfirm &>/dev/null
echo -e "  ${SR} Your system is updated."
line

# Check list packages and install if package not installed
echo -e "${RQM} Check your packages list and install if package not installed."
for pkg in "${packages[@]}"; do
	if ! pacman -Qs "$pkg" &>/dev/null; then
		echo "$PASSWD" | sudo pacman -S --noconfirm "$pkg" &>/dev/null
	fi
done
echo -e " ${SR} All packages instaled.\n"
line

# Install reflector and update mirrorlist
read "${RQM} Install reflector and update your mirrorlist? [y\n]: ${RED}" UMR
if [ "$UMR" = "y" ]; then
	echo -e " ${GCLT} Install reflector"
	echo "$PASSWD" | sudo pacman -S --noconfirm reflector &>/dev/null
	echo -e " ${GCLT} Backup your mirrorlist to mirrorlist.back in /etc/pacman.d/"
	echo "$PASSWD" | sudo mv "/etc/pacman.d/mirrorlist" "/etc/pacman.d/mirrorlist.back" &>/dev/null
	echo -e " ${GCLT} Update mirrorlist"
	echo "$PASSWD" | sudo reflector --save "/etc/pacman.d/mirrorlist" &>/dev/null
	echo -e " ${SR} All done."
	line
fi

# Install YAY
read "${RQM} Install your system YAY? [y\n]: ${RED}" YAY
if [ "$YAY" = "y" ]; then
	echo -e " ${GCLT} Install base-devel packages"
	echo "$PASSWD" | sudo pacman -S --noconfirm --needed base-devel &>/dev/null
	cd "/tmp" || return &>/dev/null
	echo -e " ${GCLT} Get YAY from ${PURPLE}https://aur.archlinux.org/yay.git"
	git clone "https://aur.archlinux.org/yay.git" &>/dev/null
	cd "yay" || return &>/dev/null
	echo -e " ${GCLT} Install YAY"
	makepkg -si &>/dev/null
	echo -e " ${SR} All done."
	line
fi

# Added users locale
read "${SR} Add your locale? [y\n]: ${RED}" LOC
if [ "$LOC" == "y" ]; then
	echo -e " ${GCLT} Generated your locale ${LOCALE}"
	sed -i "/#${LOCALE}/s/^#//g" "/etc/locale.gen" &>/dev/null
	locale-gen &>/dev/null
	echo -e " ${SR} All done."
	line
fi

# Settings SSHD
echo -e "${RQM} Start setting your SSHD."
echo -e " ${GCLT} Change default sshd port 22 to ${PURPLE}${PORTSSHD}${CYAN}."
echo $PASSWD | sudo sed -i "s/#Port 22/Port ${PORTSSHD}" "/etc/ssh/sshd_config" &>/dev/null
echo -e " ${GCLT} Desable root login in ssh."
echo $PASSWD | sudo sed -i "s/PermitRootLogin yes/PermitRootLogin no" "/etc/ssh/sshd_config" &>/dev/null
echo -e " ${GCLT} Allow login in ssh only for ${PURPLE}${USER}${CYAN}."
echo $PASSWD | sudo echo 'AllowUsers codemansrc' >>/etc/ssh/sshd_config
echo -e " ${GCLT} Restart sshd.service"
echo $PASSWD | sudo systemctl restart sshd.service &>/dev/null
echo -e " ${SR} All done."
line

# Settings IPTABLES
echo -e "${RQM} Setting SSHGUARD and IPTABLES."
echo "$PASSWD" | sudo iptables -N sshguard &>/dev/null
echo "$PASSWD" | sudo iptables -A INPUT -m multiport -p tcp --destination-ports 80,443,666 -j sshguard &>/dev/null
echo -e "  ${GCLT} Save IPTABLES rules in to ${PURPLE}/etc/iptables.rules${CYAN}."
echo "$PASSWD" | sudo bash -c "iptables-save > /etc/iptables.rules" &>/dev/null
echo -e "  ${GCLT} Enabled and restart SSHGUARD."
echo "$PASSWD" | sudo systemctl enable sshguard.service &>/dev/null
echo "$PASSWD" | sudo systemctl restart sshguard.service &>/dev/null
echo -e "  ${SR} All done."
line

# Create SWAP file
read "${SR} Enabled SWAP file? [y\n]: ${RED}" SWPF
if [ "$SWPF" == "y" ]; then
	echo -e " ${GCLT} Set size SWAP file to 512M"
	fallocate -l 512M /swapfile &>/dev/null
	echo -e " ${GCLT} Create SWAP file."
	chmod 600 /swapfile &>/dev/null
	mkswap /swapfile &>/dev/null
	swapon /swapfile &>/dev/null
	echo $PASSWD | sudo echo "/swapfile none swap defaults 0 0" >>/etc/fstab
	echo -ne "\r"
	echo -e " ${SR} All done."
	line
fi

# End
echo -e "${BLUE} Ok, we setup base setings on your VDS server."
echo -e "Now, you can make other setting!"
line
